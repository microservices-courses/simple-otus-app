package com.malik.otusapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OtusAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(OtusAppApplication.class, args);
    }

}
