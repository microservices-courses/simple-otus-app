package com.malik.otusapp.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor
@Table("users")
public class User {

    @Id
    private Long id;
    private String username;
    private String firstName;
    private String secondName;
    private String email;
    private String phone;
}
