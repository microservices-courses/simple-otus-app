package com.malik.otusapp.user;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping
    public ResponseEntity<Void> createUser(@RequestBody User user) {
        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).build();    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        return ResponseEntity.ok(user);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateUser(@PathVariable("id") long userId, @RequestBody User user) {
        User existedUser = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        if (StringUtils.hasText(user.getFirstName())) {
            existedUser.setFirstName(user.getFirstName());
        }
        if (StringUtils.hasText(user.getSecondName())) {
            existedUser.setSecondName(user.getSecondName());
        }
        if (StringUtils.hasText(user.getUsername())) {
            existedUser.setUsername(user.getUsername());
        }
        if (StringUtils.hasText(user.getEmail())) {
            existedUser.setEmail(user.getEmail());
        }
        if (StringUtils.hasText(user.getPhone())) {
            existedUser.setPhone(user.getPhone());
        }
        userRepository.save(existedUser);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable("id") long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        userRepository.delete(user);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
