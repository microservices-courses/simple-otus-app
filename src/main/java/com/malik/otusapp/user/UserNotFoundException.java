package com.malik.otusapp.user;

public class UserNotFoundException extends RuntimeException{

    private long id;

    public UserNotFoundException(long id) {
        super("User with id " + id + " is not found");
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
