FROM bellsoft/liberica-openjre-alpine-musl:11.0.11-9

WORKDIR /app

COPY target .

ENTRYPOINT java -Xmx256m -jar $(cat APP.NAME)-$(cat APP.VERS).jar
