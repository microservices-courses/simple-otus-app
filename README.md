# Homework №3

## Folders:
 * manifests/otusapp-chart - helm app chart
 * manifests/ingress - metric settings for ingress
 * manifests/db - values for bitnami/postgresql chart
 * manifests/prometheus - values for prometheus-community/kube-prometheus-stack
 * postman_test - postman test collection
 * dashboard - grafana dashboard
 * jmeter_test - simple scenario for application stress testing
